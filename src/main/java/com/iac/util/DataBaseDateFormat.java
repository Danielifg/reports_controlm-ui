package com.iac.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class DataBaseDateFormat {
	
	public static Date formatDate(String date) throws ParseException {
		
		Date formattedDate =  new SimpleDateFormat("yy-MM-dd HH:mm:ss.SSS").parse(date);
		
		return formattedDate;
	}

}
