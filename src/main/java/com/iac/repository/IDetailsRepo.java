package com.iac.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import com.iac.model.JobDetails;


@Repository
@Component
public interface IDetailsRepo  extends JpaRepository<JobDetails,Long>{

}
