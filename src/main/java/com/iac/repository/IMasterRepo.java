package com.iac.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.iac.model.JobMaster;
public interface IMasterRepo extends JpaRepository<JobMaster,Long>{

}
