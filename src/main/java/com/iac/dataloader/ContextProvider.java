package com.iac.dataloader;

import org.dataloader.DataLoaderRegistry;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ContextProvider {
	
	   public Context newContext() {
	        return new Context();
	    }

   /* final DataLoaderRegistry dataLoaderRegistry;

    @Autowired
    public ContextProvider(DataLoaderRegistry dataLoaderRegistry) {
        this.dataLoaderRegistry = dataLoaderRegistry;
    }*/
}
