package com.iac.dataloader;

import org.dataloader.DataLoaderRegistry;

import com.iac.dao.Job_MasterDAO;

/**
 * The context object is passed to each level of a graphql query and in this
 * case it contains the data loader registry. This allows us to keep our data
 * loaders per request since they cache data and cross request caches are often
 * not what you want.
 */
public class Context {

	//final private DataLoaderRegistry dataLoaderRegistry;
	final private Job_MasterDAO jMasterDAO;

	public Context() {
		this.jMasterDAO = new Job_MasterDAO();
		//this.dataLoaderRegistry = new DataLoaderRegistry();
	}

	/*public DataLoaderRegistry getDataLoaderRegistry() {
		return dataLoaderRegistry;
	}
    */
	
	public Job_MasterDAO getJob_MasterDAO() {
		return jMasterDAO;

	}

}
