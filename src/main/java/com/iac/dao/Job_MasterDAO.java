package com.iac.dao;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TemporalType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;


import com.iac.model.JobMaster;
import com.iac.repository.IMasterRepo;

@Component
public class Job_MasterDAO {

	@Autowired
	private  EntityManager em;

	@Transactional
	public  List<JobMaster> getJobMasterRecordsByDate(String from, String to) throws ParseException {
	
		Date date1 = new SimpleDateFormat("yy-MM-dd HH:mm:ss.SSS").parse(from + " 00:00:00.000");
		Date date2 = new SimpleDateFormat("yy-MM-dd HH:mm:ss.SSS").parse(to + " 00:00:00.000");
		String i = "";
		
		Query query = em.createQuery("SELECT e FROM JobMaster e where e.exctnStrtTm BETWEEN :start AND :end");
		
		query.setParameter("start", date1, TemporalType.TIMESTAMP).setParameter("end", date2, TemporalType.TIMESTAMP);
	
	
		@SuppressWarnings("unchecked")
		List<JobMaster> jobMaster =  query.getResultList();
	
			
		return jobMaster;
	
	}
}