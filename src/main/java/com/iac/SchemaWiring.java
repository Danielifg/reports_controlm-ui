package com.iac;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.iac.dao.Job_MasterDAO;
import com.iac.dataloader.Context;
import com.iac.model.JobMaster;

import graphql.schema.DataFetcher;
import graphql.schema.DataFetchingEnvironment;


@Component
public class SchemaWiring {

@Autowired
 Job_MasterDAO jobMasterDAO;
	
	 DataFetcher<?> customersFetcher = env -> {
		  Context ctx = env.getContext();
		  
		  List<JobMaster> result = new ArrayList<JobMaster>();
		  
			try {
				result = jobMasterDAO
							.getJobMasterRecordsByDate(env.getArgument("from"), env.getArgument("to"));												
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			return result;
	    };


	static DataFetcher<?> getJobsFetcher = new DataFetcher<Object>() {
		
		@Override
		public Object get(DataFetchingEnvironment environment) {
			List<JobMaster> result = new ArrayList<JobMaster>();
			Context ctx = environment.getContext();
			try {
				result = ctx.getJob_MasterDAO()
							.getJobMasterRecordsByDate(environment.getArgument("from"), environment.getArgument("to"));
				
						//jMasterDAO.getJobMasterRecordsByDate(environment.getArgument("from"), environment.getArgument("to"));
				
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			return result;
		}
	};
	
   DataFetcher<?> getJobReports = env ->{
		Context ctx = env.getContext();
		String from = env.getArgument("from");
		String to = env.getArgument("to");
		List<JobMaster> jobMaster = new ArrayList<>();
		//ctx.getJob_MasterDAO().getJobMasterRecordsByDate(from, to);
		try {
			jobMaster =  ctx.getJob_MasterDAO().getJobMasterRecordsByDate(from, to);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			System.out.println("ERROR ----//---->"+ e.getMessage());
			
			e.printStackTrace();
		}
		return jobMaster;
				
		
	};



}
