package com.iac.controller;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Charsets;
import com.google.common.io.Resources;
import com.iac.SchemaWiring;
import com.iac.dao.Job_MasterDAO;
import com.iac.dataloader.Context;
import com.iac.dataloader.ContextProvider;
import com.iac.model.JobMaster;
import com.iac.repository.IMasterRepo;

import graphql.ExecutionInput;
import graphql.ExecutionResult;
import graphql.GraphQL;
import graphql.GraphQLError;
import graphql.schema.DataFetcher;
import graphql.schema.DataFetchingEnvironment;
import graphql.schema.GraphQLSchema;
import graphql.schema.idl.RuntimeWiring;
import graphql.schema.idl.SchemaGenerator;
import graphql.schema.idl.SchemaParser;
import graphql.schema.idl.TypeDefinitionRegistry;
/*
@RestController
@RequestMapping("/masterTable")
@CrossOrigin(origins = "http://localhost:3000")
public class Job_MasterController {

	@Autowired
	Job_MasterDAO jobMasterDAO;

	@Autowired
	IMasterRepo masterRepo;

	@Value("classpath:types.graphqls")
	private Resource schemaResource;

	private GraphQL graphql;
	private final ObjectMapper objectMapper;
	private Context ctx;

	public Job_MasterController(GraphQL graphql, ObjectMapper objectMapper) {
		this.graphql = graphql;
		this.objectMapper = objectMapper;
	}

	@PostConstruct
	public void loadSchema() throws IOException {

		File schemaFile = schemaResource.getFile();
		TypeDefinitionRegistry registry = new SchemaParser().parse(schemaFile);
		RuntimeWiring wiring = buildWiring();
		GraphQLSchema schema = new SchemaGenerator().makeExecutableSchema(registry, wiring);
		graphql = GraphQL.newGraphQL(schema).build();

	}

	private RuntimeWiring buildWiring() {

		DataFetcher<?> fetcher2 = new DataFetcher<Object>() {
			@Override
			public Object get(DataFetchingEnvironment environment) {
				List<JobMaster> result = new ArrayList<JobMaster>();
				ctx = environment.getContext();
				try {
					result = jobMasterDAO.getJobMasterRecordsByDate(environment.getArgument("from"),
							environment.getArgument("to"));
					// jMasterDAO.getJobMasterRecordsByDate(environment.getArgument("from"),
					// environment.getArgument("to"));
				} catch (ParseException e) {
					e.printStackTrace();
				}
				return result;
			};
		};

		return RuntimeWiring.newRuntimeWiring()
				.type("Query", typeWriting -> typeWriting.dataFetcher("getJobs", fetcher2)).build();
	}

	@GetMapping("/dates/{starts}/{ends}") public List<JobMaster>
	  findJobDetailsByDate(@PathVariable @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
	  String starts,@PathVariable @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) String ends)
	  throws ParseException {
	   return null; // jobMasterDAO.getJobMasterRecordsByDate(starts, ends); 
	  }

	@PostMapping("/getJobs")
	public ResponseEntity<Object> getJobs(@RequestBody String query) {
		ExecutionResult result = graphql.execute(query);
		return new ResponseEntity<Object>(result, HttpStatus.OK);
	}

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/graphql", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@CrossOrigin
	public void graphql(@RequestBody Map<String, Object> body, HttpServletRequest httpServletRequest,
			HttpServletResponse httpServletResponse) throws IOException {

		String query = (String) body.get("query");
		if (query == null) {
			query = "";
		}
		String operationName = (String) body.get("operationName");
		Map<String, Object> variables = (Map<String, Object>) body.get("variables");
		if (variables == null) {
			variables = new LinkedHashMap<String, Object>();
		}
		executeGraphqlQuery(httpServletResponse, operationName, query, variables);
	}

	private void executeGraphqlQuery(HttpServletResponse httpServletResponse, String operationName, String query,

			Map<String, Object> variables) throws IOException {

		ctx = new Context();

		ExecutionInput executionInput = ExecutionInput.newExecutionInput().query(query).variables(variables)
				.operationName(operationName).context(ctx).build();

		ExecutionResult executionResult = graphql.execute(executionInput);
		handleNormalResponse(httpServletResponse, executionResult);
	}

	private void handleNormalResponse(HttpServletResponse httpServletResponse, ExecutionResult executionResult)
			throws IOException {
		Map<String, Object> result = executionResult.toSpecification();
		httpServletResponse.setStatus(HttpServletResponse.SC_OK);
		httpServletResponse.setCharacterEncoding("UTF-8");
		httpServletResponse.setContentType("application/json");
		httpServletResponse.setHeader("Access-Control-Allow-Origin", "*");
		String body = objectMapper.writeValueAsString(result);
		PrintWriter writer = httpServletResponse.getWriter();
		writer.write(body);
		writer.close();
	}

	@GetMapping("/test")
	public String findJobDetailsByDate() {
		return "works";
		}
	
}*/
