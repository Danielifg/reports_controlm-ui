package com.iac;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collection;

import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TemporalType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.Transactional;

import com.iac.model.JobMaster;
import com.iac.repository.IDetailsRepo;

@SpringBootApplication(scanBasePackages={"com.iac"})
@EnableJpaAuditing
@EnableJpaRepositories("com.iac.repository")
public class ReportsControlMUI {
	public static void main(String[] args) throws ParseException {
		SpringApplication.run(ReportsControlMUI.class, args);
	}
	
}











