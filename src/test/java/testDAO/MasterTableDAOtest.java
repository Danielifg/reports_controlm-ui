package testDAO;

import static org.junit.Assert.*;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TemporalType;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.iac.dao.Job_MasterDAO;
import com.iac.model.JobMaster;
import com.iac.util.DataBaseDateFormat;

public class MasterTableDAOtest {
	
   
	@Autowired
	private EntityManager em;

    
	@Test
	public void getJobMasterRecordsByDateTest() throws ParseException {

		String from = "18-08-17";
		String to ="18-09-16";
		
		Job_MasterDAO masterDAO = new Job_MasterDAO();
	
		Assert.assertEquals(109797, masterDAO.getJobMasterRecordsByDate(from, to).size());
	}

}
